package com.example.core.today;



import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;


public class WholeArticle extends Activity {
	 
	private WebView webView;
	private ProgressBar mPBar;
	 
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.whole_article);
		
		Intent intent = new Intent(this.getIntent());
		String curDesc;
		curDesc = intent.getStringExtra("desc");			
		webView = (WebView) findViewById(R.id.webView1);
		mPBar = (ProgressBar) findViewById(R.id.progress01);
		WebSettings set = webView.getSettings();
		
		webView.getSettings().setJavaScriptEnabled(true);
		webView.loadUrl(curDesc); // 보여주고자 하는 주소
		
		set.setCacheMode(WebSettings.LOAD_NO_CACHE);
		set.setSupportZoom(false);
		
		webView.setWebViewClient(new WebClient());
 
	
 
	webView.setWebChromeClient(new WebChromeClient()        
	 {
		 public void onProgressChanged(WebView view, int progress) {
			 if (progress<100)
			 {
				 mPBar.setVisibility(ProgressBar.VISIBLE);                   
			 }                   
			 else if (progress==100)                   
			 {                       
				 mPBar.setVisibility(ProgressBar.GONE);                   
			 }                   
			 mPBar.setProgress(progress);                 
			 }           
		
	
				
	 
	 });}
	 
	 @Override
	    public boolean onKeyDown(int keyCode, KeyEvent event){
	        if(keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0){
	            if(webView.canGoBack()){
	            	webView.goBack();
	            }else{
	            	webView.clearCache(false);
	                finish();
	            }
	            return true;
	        }
	        return super.onKeyDown(keyCode, event);
	
	 	}
	 
	 private class WebClient extends WebViewClient { 
	        @Override 
	        public boolean shouldOverrideUrlLoading(WebView view, String url) {
	            
	        	if (url.startsWith("sms:")) {
            	     Intent i = new Intent(Intent.ACTION_SENDTO, Uri.parse(url));
            	     startActivity(i);
            	     return true;
            	    }
	        	
	        	if (url.startsWith("kakaolink:")) {
            	     Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            	     startActivity(i);
            	     return true;
            	    }
	        	      	
	        			        	
	        	if(url.startsWith("tel")){
	                    Intent i = new Intent(Intent.ACTION_DIAL);
	                    i.setData(android.net.Uri.parse(url));
	                    startActivity(i);
	                    
	            } else {
	                    view.loadUrl(url);
	                    
	            } 
	          
	            return true;
	   
	    }}		 
}