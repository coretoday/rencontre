package com.example.core.today;

import java.util.ArrayList;




public class JsonNewsItem {
	private String keyword;
	private String article;
	private String event1;
	private String event2;
	private String event3;
	public boolean isOpen;
	
	/**
	 * Initialize with icon and data array
	 */
	public JsonNewsItem() {
		
	}
	
	public JsonNewsItem(String a) {
		
	}

	/**
	 * Initialize with icon and strings
	 */
	public JsonNewsItem(String keyword, String article, String event1,String event2, String event3) {
		this.keyword = keyword;
		this.article =article;
		this.event1=event1;
		this.event2=event2;
		this.event3=event3;
		this.isOpen=false;
		
	}
	

	public String getTitle() {
		return keyword;
	}

	public void setTitle(String title) {
		this.keyword = title;
	}

	public String getLink() {
		return article;
	}

	public void setLink(String link) {
		this.article = link;
	}
	
	public boolean getisOpen(){
		return isOpen;
	}

	public void setisOpen(boolean isOpen) {
		this.isOpen = isOpen;
	}
	
	public String getEvent1(){
		return event1;
	}
	
	public void setEvent1(String event1){
		this.event1=event1;
	}
	
	public String getEvent2(){
		return event2;
	}
	
	public void setEvent2(String event2){
		this.event2=event2;
	}
	
	public String getEvent3(){
		return event3;
	}
	
	public void setEvent3(String event3){
		this.event3=event3;
	}

	/**
	 * Compare with the input object
	 * 
	 * @param other
	 * @return
	 */
	public boolean compareTo(JsonNewsItem other) {
		if (keyword.equals(other.getTitle())) {
			return false;
		} else if (article.equals(other.getLink())) {
			return false;
		} 
		
		return true;
	}

}
