package com.example.core.today;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.R.color;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.webkit.WebView.FindListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;


public final class TestFragment extends Fragment implements OnRefreshListener<ListView>, OnItemClickListener{
    private static final String KEY_CONTENT = "TestFragment:Content";
	private static final int REQUEST_CODE_ANOTHER = 000;
    private String mContent = "???";
    private static String _content;
    int click_focus = -1; 
    int itemcount=0;


    Context context = null;
    Animation scaleUpAnimation;
    Animation scaleDownAnimation;
    JsonNewsAdapter adapter;
    PullToRefreshListView plv = null;
    String u="u";
    
    
    public static TestFragment newInstance(String content) {
        TestFragment fragment = new TestFragment();
        _content=content;
        return fragment;
    }

    

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if ((savedInstanceState != null) && savedInstanceState.containsKey(KEY_CONTENT)) {
            mContent = savedInstanceState.getString(KEY_CONTENT);
        }
        
	    
    }

    public ArrayList<JsonNewsItem> Parsing (int max){
    	                         
    	String line;
		String page ="k";
		String keyword="a",article="b",event1="c",event2="d",event3="e";

		final String group = ("http://kr.core.today/json/?d=20141101&n=30");


		JSONObject test;
		BufferedReader bufreader = null;
		HttpURLConnection urlConnection = null;
		JSONArray alljson;
		URL url1 = null;
		ArrayList<JsonNewsItem> m_orders = new ArrayList<JsonNewsItem>();
		
			
			page = "";
		
		try{
			String sample = group;
			url1 = new URL(sample);
			urlConnection = (HttpURLConnection) url1.openConnection();

			bufreader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(),"UTF-8"));

			while((line=bufreader.readLine())!=null){
				page+=line;
			}

			alljson = new JSONArray(page);

		for(int k =0; k < max ; k++){
			test=new JSONObject(alljson.getString(k));
			keyword=test.getString("k");
			article=test.getString("s");
			/*event1 = keyword.split(" ")[0];
	        event2 = keyword.split(" ")[1];
	        event3 = keyword.split(" ")[2];*/
			m_orders.add(new JsonNewsItem(keyword,article,event1,event2,event3));
						
		}
		
		}
		catch(Exception e){
			Toast.makeText(context, "Fail to parse JSON", 1000).show();
		}
		/////////////////////////////////////////////////////////////////////////////////////////////parse JSON file
		
		urlConnection.disconnect();
		
		
		
		
		
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		///////////////////////////////////////////////////////////////////////////////add NewsItem to ArrayList

		
		
		return m_orders;
    }
    
    public LinearLayout listmaker(PullToRefreshListView plv, Context mcontext, ViewGroup container){
		plv = (PullToRefreshListView) LayoutInflater.from(context).inflate(
		R.layout.layout_listview_in_viewpager, container, false);
		adapter = new JsonNewsAdapter(context, R.layout.listitem , Parsing(30));
		plv.setAdapter(adapter);
		plv.setOnItemClickListener(this);
		plv.setOnRefreshListener(TestFragment.this); /// set Refresh listener
		
		LinearLayout layout = new LinearLayout(getActivity());
	    layout.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
	    layout.setGravity(Gravity.CENTER);
		layout.addView(plv);
		/////////////////////////////////////////////////////////////////////////////inflate layout and listview
			return layout;
		}
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

			context = container.getContext(); // get Context
			
			
			
			
			 
			
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////
			StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectDiskReads().detectDiskWrites().detectNetwork().penaltyLog().build());
			
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
				
			return listmaker(plv, context, container);
		

    }
    
    public class NewsViewHolder{
    	TextView keyword;
    	TextView article;
    	TextView event1;
    	TextView event2;
    	TextView event3;
    	boolean isOpen;
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////
    public class JsonNewsAdapter extends ArrayAdapter<JsonNewsItem> {
		private ArrayList<JsonNewsItem> items;

		public JsonNewsAdapter(Context context, int textViewResourceId, ArrayList<JsonNewsItem> items) {
			super(context, textViewResourceId, items);
			this.items = items;
		}
	    
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {    
			int count;
			LayoutParams lp = null;
			View v = convertView;  
			NewsViewHolder holder = null;

			if (v == null) {      
				LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);     
	                	v = vi.inflate(R.layout.listitem, null);
	                	holder = new NewsViewHolder();
	                	holder.keyword= (TextView) v.findViewById(R.id.dataItem01);
	                	holder.article= (TextView) v.findViewById(R.id.dataItem02);
	                	holder.event1 = (TextView) v.findViewById(R.id.event1);
	                	holder.event2 = (TextView) v.findViewById(R.id.event2);
	                	holder.event3 = (TextView) v.findViewById(R.id.event3);
	                	holder.isOpen=false;
	                	
			}
			else
			{
				holder=(NewsViewHolder)v.getTag();
			}
			
			JsonNewsItem p = items.get(position);     

			v.setTag(holder);
			View toolbar = v.findViewById(R.id.toolbar);
			lp=(LayoutParams) toolbar.getLayoutParams();
			if(holder.isOpen){
				toolbar.setVisibility(View.GONE);
				lp.bottomMargin=0- v.getHeight();
				v.requestLayout();
			}
			
			if(p.isOpen){
				toolbar.setVisibility(View.VISIBLE);
				lp.bottomMargin=0;
				v.requestLayout();
			}

			
			if(p!=null){
				holder.keyword.setText(p.getTitle());
				holder.article.setText(p.getLink());
				holder.event1.setText(p.getEvent1());
				holder.event2.setText(p.getEvent2());
				holder.event3.setText(p.getEvent3());
				holder.isOpen=p.getisOpen();
			}
		return v;   
		} 
	}
    ///////////////////////////////////////////////////////////////////////listview adapter
    
    
    
    
    
    
    
    /////////////////////////////////////////////////////////////////////////////////////
    @Override
	public void onRefresh(PullToRefreshBase<ListView> refreshView) {
		new GetDataTask(refreshView).execute();
		
	}

	private static class GetDataTask extends AsyncTask<Void, Void, Void> {

		PullToRefreshBase<?> mRefreshedView;

		public GetDataTask(PullToRefreshBase<?> refreshedView) {
			mRefreshedView = refreshedView;
		}

		@Override
		protected Void doInBackground(Void... params) {
			// Simulates a background job.
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			
			
			mRefreshedView.onRefreshComplete();
			
			
			super.onPostExecute(result);
		}
	}
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_CONTENT, mContent);
    }
    ////////////////////////////////////////////////////////////the task done during refreshed






    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
          long id) {
    	
    	View toolbar = view.findViewById(R.id.toolbar);
    	NewsViewHolder holder = null;
    	JsonNewsItem p = adapter.getItem(position-1);
    	if(p.isOpen){
    		p.isOpen=false;  		
    	}
    	else{
    		p.isOpen=true;
    	}
    	
    	
    	holder = new NewsViewHolder();
    	holder.keyword= (TextView) view.findViewById(R.id.dataItem01);
    	holder.article= (TextView) view.findViewById(R.id.dataItem02);
    	holder.event1 = (TextView) view.findViewById(R.id.event1);
    	holder.event2 = (TextView) view.findViewById(R.id.event2);
    	holder.event3 = (TextView) view.findViewById(R.id.event3);

    	holder.isOpen=p.getisOpen();
    	view.setTag(holder);
    	
    	
        // Creating the expand animation for the item
        ExpandAnimation expandAni = new ExpandAnimation(toolbar, 500);
       
        toolbar.startAnimation(expandAni);
    
        // Start the animation on the toolbar
        

    }
 
    
    
}
